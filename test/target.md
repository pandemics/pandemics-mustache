---
mustache:
  - view1.yaml
  - prefix: sub
    file: sub/*
  - pairs.mustache
---

There is a simple value1, and two nested globs value2 and value3.

- **item 1**: description 1
- **item 2**: description 2
