---
mustache: 
- view1.yaml
- prefix: sub
  file: sub/*
- pairs.mustache
---

There is a simple {{ label1 }}, and two nested globs {{ sub.label1 }} and {{ sub.label2 }}.

{{#dictionary}}
{{> pairs}}
{{/dictionary}}
