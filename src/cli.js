#!/usr/bin/env node
const fs = require ('fs');
const path = require ('path');
const pandemicsMustache = require('./index.js')

// read from stdin by default except if markdown passed as first argument
let input = 0
if (process.argv[2] && path.extname (process.argv[2]) == ".md") {
  input = process.argv[2];
  console.error(`processing file: ${input}`);
}

// collects views and partials if passed as arguments
const element1st = 2 + (input !== 0)
let elements = []
if (process.argv.length > element1st) {
  elements = process.argv.splice (element1st)
  console.error(`using views: ${elements.join()}`);
}

// process
let options = {}
if (input !== 0) {
  options.basePath = path.resolve (process.cwd (), path.dirname (input))
}

try {
  var doc = pandemicsMustache (fs.readFileSync (input, 'utf8'), elements, options);
  // write to the pipe
  process.stdout.write (doc);
} catch (err) {
  console.error (`*** pandemics-mustache: ${err.message}`)
}
