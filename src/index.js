#!/usr/bin/env node
const fg = require('fast-glob');
const fs = require('fs');
const jsyaml = require('js-yaml');
const mdescape = require('markdown-escape');
const mustache = require('mustache')
const path = require('path');
const yamlFront = require('yaml-front-matter');

function pandemicsMustache (doc, elements, options = {}) {
// #############################################################################
// prepare content
// #############################################################################

// split content from front matter
const frontMatter = yamlFront.loadFront (doc)
const content = frontMatter.__content
delete frontMatter.__content

const sourcePath = options.basePath || process.env.PANDOC_SOURCE_PATH || process.cwd ()

// #############################################################################
// collect views and partials: arguments > front-matter
// #############################################################################
if (! elements.length){
  // look in the front matter for views to use, ensuring it's an array
  elements = [].concat (frontMatter.mustache || []);
  console.error(`loading elements from front-matter`);
}

// perform glob expansion for all elements, duplicating prefixes and tags
elements = elements.flatMap (function (e) {
  // just file or glob
  if (typeof(e) === 'string') {
    // expand glob
    let fileList = fg.sync (e, {cwd: sourcePath})
    // add prefix or tag according to accepted file type, ignore other files
    fileList = fileList.map (function (f) {
      if (['.yaml', '.yml', '.json'].includes (path.extname (f))) {
        return {prefix: null, file: f}
      } else if (path.extname (f) == '.mustache') {
        return {tag: null, file: f}
      }
    })
    // check results were found
    if (! fileList.length) {
      throw new Error (`cannot find files matching ${e}`)
    }
    console.error (`expanding: ${e}... ${JSON.stringify(fileList)}`)
    return fileList
  // already objects
  } else if (typeof(e) === 'object') {
    let fileList = fg.sync(e.file, {cwd: sourcePath}).map((f) => Object.assign({}, e, { file: f }))
    console.error (`expanding: ${e.file}... ${JSON.stringify(fileList)}`)
    return fileList
    //#TODO: filter files matching label type
  }
})

// if no views, return untouched content
if (! elements.length) {
  return doc;
}

// #############################################################################
// function to merge a view into an existing one with conflict detection
// #############################################################################
function extend_view (target, source) {
  // for every new property to add
  Object.keys (source).forEach (key => {
    // if new, just include in target
    if (! target.hasOwnProperty (key)) {
      target[key] = source[key];
    // if not new but object, recurse and bubble up conflicts
    } else if (typeof source[key] === 'object') {
      try {
        extend_view (target[key], source[key])
      } catch (err) {
        throw new Error (key + '.' + err.message)
      }
    // conflict! throw key name
    } else {
      throw new Error (key)
    }
  })
}

// #############################################################################
// merge all views together
// #############################################################################
var values
var views = {}
elements
  // exclude partials
  .filter (e => e.prefix !== undefined)
  // iteratively add values from each view
  .forEach (view => {
    // get full path
    // -------------------------------------------------------------------------
    filePath = path.join (sourcePath, view.file);
    console.error (`> loading mustache view${view.prefix ? ' (prefix = '+ view.prefix+')' : ''}: ${view.file}`);
    // load as json with prefix if necessary
    // -------------------------------------------------------------------------
    values = jsyaml.load (fs.readFileSync (filePath, 'utf8'));
    if (view.prefix) {
      values = Object.fromEntries ([[view.prefix, values]])
    }
    console.error (`${JSON.stringify (values, null, 2)}\n`);
    // merge to the general view
    // -------------------------------------------------------------------------
    try {
      extend_view (views, values)
    } catch (err) {
      throw new Error (`cannot merge mustache view, field ${err.message} already exist.`)
    }
  });

console.error (`> merged view:\n${JSON.stringify(views,null,2)}\n`);

// #############################################################################
// collect all partials
// #############################################################################
var template
var partials = {}
elements
  // exclude partials
  .filter (e => e.tag !== undefined)
  // iteratively add values from each view
  .forEach (partial => {
    // get full path
    // -------------------------------------------------------------------------
    filePath = path.join (sourcePath, partial.file);
    console.error (`> loading mustache partial${partial.tag ? ' (tag = '+ partial.tag+')' : ''}: ${partial.file}`);
    // load as json with prefix if necessary
    // -------------------------------------------------------------------------
    template = fs.readFileSync (filePath, 'utf8');
    let tag = partial.tag || path.parse(partial.file).name
    partials[tag] = template
    // merge to the general view
    // -------------------------------------------------------------------------
  });

console.error (`> partial templates:\n${JSON.stringify(partials,null,2)}\n`);


// #############################################################################
// redefine mustache symbol lookup to fail on missing dict entry
// #############################################################################
var lookup = mustache.Context.prototype.lookup;
mustache.Context.prototype.lookup = function (name) {
    var value = lookup.bind (this) (name);
    if (value === undefined) {
      if (frontMatter.pandemics && frontMatter.pandemics.strict) {
        throw new Error (`unknown symbol '${name}'.`)
      } else {
        value = '**???**'
      }
    }
    return value;
}

// #############################################################################
// return processed source
// #############################################################################
  let mustached_content = mustache.render (content, views, partials);
  let output = `---\n`
      + jsyaml.dump(frontMatter)
      + `---`
      + mustached_content;
  return output;

}

module.exports = pandemicsMustache