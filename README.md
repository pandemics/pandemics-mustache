# pandemics-mustache

Wrapper to apply mustache templating to your manuscript in pandemics.

## Usage

```sh
cat my_document > pandemics-mustache [view1.json] [view2.json] [...]
```

Where the arguments are .json or .yaml files that contain the 'views' (ie. the name/values association to replace in the template).

If no arguments are given, pandemics-mustache will look in the front matter of the document provided on stdin:

```yaml
---
mustache: results.json
---
```

or

```yaml
---
mustache:
- results/res1.json
- results/res2.yaml
---
```

### Glob expansion

You can directly load a set of files using globs:

```yaml
---
mustache: results/*
---
```

### Conflict management

If multiple files are provided, it might happen that the same entry is defined twice, which will throw an error.
One workaround is add a prepend a prefix to the values loaded from a file, eg:

```yaml
---
mustache:
- results/res1.json
- prefix: newres
  file: results/res2.yaml
---
```

will make all values in `results/res2.yaml` available using `{{ newres.keyinres2 }}`.
